package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;
import sheridan.MealsService;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		assertTrue("MealType.DRINKS is NOT empty", MealType.DRINKS != null);       
	}
	@Test
	public void testDrinksException() {
		assertFalse("MealType.DRINKS is empty", MealType.DRINKS == null); 
	}
	@Test
	public void testDrinksBoundaryIn() {
		assertTrue("MealType.DRINKS is NOT empty", MealType.DRINKS != null);
	}
	@Test
	public void testDrinksBoundaryOut() {
		assertFalse("MealType.DRINKS is NOT empty", MealType.DRINKS == null);      
	}

}
